import java.util.Scanner;

public class XO {
	static String[][] Table = { {"-","-","-"} ,
			 				    {"-","-","-"} ,
			 				    {"-","-","-"} ,};
	static String player; 
	static int column ,row ,num = 0;
	static Scanner max = new Scanner(System.in);
	
	static void startXO() {
		System.out.println("Welcome To XO Game");
		startPlayer();
	}
	
	 static void startPlayer() {
		 System.out.print("Start x or o : ");
		 player = max.next();
		 if(player.equals("x")||player.equals("o")) {
			 printTable();
		 }else {
			 System.out.println("try again!!");
			 startPlayer();
		 }
	}
	 
	  static void input() {
		    System.out.println("Turn "+player);
			System.out.println(player+"(row,column) : ");
			row = max.nextInt()-1;
			column = max.nextInt()-1;
			if((row >= 0 && row <= 2)&&(column >= 0 && column <= 2)) {

				if(Table[row][column].equals("-")) {
					Table[row][column] = player;
					printTable();
					num++;
				}else {
					System.out.println("try again!!");
					System.out.println();
					input();
				}
			}else {
				System.out.println("input try again!!");
				input();
			}
			
	}
	  
	  static void switchPlayer(){
		  if(player.equals("x")) {
				player = "o";
			}else if(player.equals("o")) {
				player = "x";
			}
	  }
	  
	  public static void printTable() {
			System.out.println("  1 2 3");
			for(int i =0;i<3;i++) {
				System.out.print((i+1)+" ");
				for(int j =0;j<3;j++) {
					System.out.print(Table[i][j]+" ");
				}
				System.out.println();
			}
			System.out.println();
		}
	 
	
	public static void main(String[] args) {
		startXO();
		for(;;) {
			input();
			if(checkWin()){
				break;
			}
			if(checkDraw()) {
				break;
			}
			switchPlayer();
		}
		System.out.println("Bye Bye!!");
	}
	
	static boolean checkDraw() {
		if(num == 9 ) {
			return true;
		}
		return false;
	}
	
	public static boolean checkWin() {
		for(int i=0;i<3 ;i++) {
			if(Table[i][0].equals(Table[i][1]) && Table[i][1].equals(Table[i][2])) {
				if(!Table[i][0].equals("-") && !Table[i][1].equals("-") && !Table[i][2].equals("-")){
					System.out.println(Table[i][0]+" win!!");
					return true;
				}
			}if(Table[0][i].equals(Table[1][i]) && Table[1][i].equals(Table[2][i])) {
				if(!Table[0][i].equals("-") && !Table[1][i].equals("-") && !Table[2][i].equals("-")) {
					System.out.println(Table[0][i]+" win!!");
					return true;
				}
			}
		}
		if(Table[0][0].equals(Table[1][1]) && Table[1][1].equals(Table[2][2])) {
			if(!Table[0][0].equals("-") && !Table[1][1].equals("-") && !Table[2][2].equals("-")) {
				System.out.println(Table[0][0]+" win!!");
				return true;
			}
		}
		if(Table[0][2].equals(Table[1][1]) && Table[1][1].equals(Table[2][0])) {
			if(!Table[0][2].equals("-") && !Table[1][1].equals("-") && !Table[2][0].equals("-")) {
				System.out.println(Table[0][0]+" win!!");
				return true;
			}
		}
		return false;
	}

}
